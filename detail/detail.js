// detail.js
Page({
  data: {
    detailContent: ''
  },
  
  onLoad: function (options) {
    // options 中包含传递的参数
    const postId = options.id; // 假设传递的参数是 id
    // 根据 postId 从服务器获取详情内容，这里假设获取到了详情内容
    const detailContent = '详情内容：' + postId;
    this.setData({
      detailContent: detailContent
    });
  },

  // 在详情页的页面参数中，通过 onLoad 获取传递过来的网页地址
onLoad: function(options) {
  // 获取传递过来的网页地址
  var webUrl = options.url;
  // 将网页地址保存到页面数据中
  this.setData({
    webUrl: webUrl
  });
},
// 打开网页
openWebPage: function() {
  // 获取保存的网页地址
  var webUrl = this.data.webUrl;
  // 使用小程序内置的组件打开网页
  wx.navigateTo({
    url: '/pages/webpage/webpage?url=' + webUrl,
  });
},

});
