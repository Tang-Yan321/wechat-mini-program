// pages/main page/main page.js
Page({
  // 悬停效果和点击效果
  showFullScreenImage: function (e) {
    const url = e.currentTarget.dataset.url;
    const allImages = this.data.natureimage.concat(this.data.animalimage).map(item => item.url);
    wx.previewImage({
      current: url,
      urls: allImages
    });
  },

  swiperChange: function (e) {
    const direction = e.currentTarget.dataset.direction;
    if (direction === 'prev') {
      this.selectComponent('#swiper').previous();
    } else if (direction === 'next') {
      this.selectComponent('#swiper').next();
    }
  },
  // 第二板块 show-container
  data: {
    natureimage: [{
        id: 1,
        url: '../../images/nature1.jpg',
        alt: 'Image 1'

      },
      {
        id: 2,
        url: '../../images/nature2.jpg',
        alt: 'Image 2'
      },
      {
        id: 3,
        url: '../../images/nature3.jpg',
        alt: 'Image 3'
      },
      {
        id: 4,
        url: '../../images/nature4.jpg',
        alt: 'Image 4'
      },
      {
        id: 5,
        url: '../../images/nature5.jpg',
        alt: 'Image 5'
      },
      {
        id: 6,
        url: '../../images/nature6.jpg',
        alt: 'Image 6'
      },
    ],

    animalimage: [{
        id: 1,
        url: '../../images/animal-1.jpg',
        alt: 'Image 1'

      },
      {
        id: 2,
        url: '../../images/animal-2.jpg',
        alt: 'Image 2'
      },
      {
        id: 3,
        url: '../../images/animal-3.jpg',
        alt: 'Image 3'
      },
      {
        id: 4,
        url: '../../images/animal-4.jpg',
        alt: 'Image 4'
      },
      {
        id: 5,
        url: '../../images/animal-5.jpg',
        alt: 'Image 5'
      }, {
        id: 6,
        url: '../../images/animal-6.jpg',
        alt: 'Image 6'
      },
    ],

    peopleimage: [{
        id: 1,
        url: '../../images/people-1.jpg',
        alt: 'Image 1'

      },
      {
        id: 2,
        url: '../../images/people-2.jpg',
        alt: 'Image 2'
      },
      {
        id: 3,
        url: '../../images/people-3.jpg',
        alt: 'Image 3'
      },
      {
        id: 4,
        url: '../../images/people-4.jpg',
        alt: 'Image 4'
      },
      {
        id: 5,
        url: '../../images/people-5.jpg',
        alt: 'Image 5'
      }, {
        id: 6,
        url: '../../images/people-6.jpg',
        alt: 'Image 6'
      },
    ],

    animeimage: [{
        id: 1,
        url: '../../images/anime_1.jpg',
        alt: 'Image 1'

      },
      {
        id: 2,
        url: '../../images/anime_2.png',
        alt: 'Image 2'
      },
      {
        id: 3,
        url: '../../images/anime_3.png',
        alt: 'Image 3'
      },
      {
        id: 4,
        url: '../../images/anime_4.jpg',
        alt: 'Image 4'
      },
      {
        id: 5,
        url: '../../images/anime_5.png',
        alt: 'Image 5'
      }, {
        id: 6,
        url: '../../images/anime_6.png',
        alt: 'Image 6'
      },
    ],
    cityimage: [{
        id: 1,
        url: '../../images/city_1.png',
        alt: 'Image 1'

      },
      {
        id: 2,
        url: '../../images/city_2.jpg',
        alt: 'Image 2'
      },
      {
        id: 3,
        url: '../../images/city_3.jpg',
        alt: 'Image 3'
      },
      {
        id: 4,
        url: '../../images/city_4.jpg',
        alt: 'Image 4'
      },
      {
        id: 5,
        url: '../../images/city_5.jpg',
        alt: 'Image 5'
      }, {
        id: 6,
        url: '../../images/city_6.jpg',
        alt: 'Image 6'
      },
    ],
  },

  // 触底加载新数据


  //跳转nature页面
  natureshow: function () {
    wx.navigateTo({
      url: '/pages/nature/nature'
    });
  },
  //跳转animal页面
  animalshow: function () {
    wx.navigateTo({
      url: '/pages/animal/animal'
    });
  },
  //跳转people页面
  peopleshow: function () {
    wx.navigateTo({
      url: '/pages/people/people'
    });
  },
  //跳转anime页面
  animeshow: function () {
    wx.navigateTo({
      url: '/pages/anime/anime'
    });
  },
  //跳转city页面
  cityshow: function () {
    wx.navigateTo({
      url: '/pages/city/city'
    });
  },
  //跳转gallery页面
  galleryshow: function () {
    wx.navigateTo({
      url: '/pages/gallery/gallery'
    });
  },
});