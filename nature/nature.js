//pages/nature/nature.js
Page({
  data: {
    photoList: [
      { url: '../../images/nature-tress.jpg' },
      { url: '../../images/nature-road.jpg' },
      { url: '../../images/nature-flowers.jpg' },
      { url: '../../images/nature-avenue.jpg' },
      { url: '../../images/forest-438432_1280.jpg' },
      { url: '../../images/forest-1072828_1280.jpg' },
      // 其他照片URL
    ]
  }
})

