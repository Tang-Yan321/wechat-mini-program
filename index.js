Page({
  data: {
    imgUrls: [
      '../../../images/camera-2.jpg',
      '../../../images/camera-3.jpg',
      '../../../images/camera-5.jpg'
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 3000,
    duration: 800,
  },

  data: {
    selectedIndex: 0, // 当前选中的页面索引
    pages: [ // 页面列表
      {
        iconPath: "../../../images/12.png",
        selectedIconPath: "../../../images/11.png",
        text: "主页",
        path: "/pages/mall/component/index" // 首页的路径
      },
      {
        iconPath: "../../../images/22.png",
        selectedIconPath: "../../../images/21.png",
        text: "分类",
        path: "/pages/mall/component/category/category" // 分类页面的路径
      },
      {
        iconPath: "../../../images/32.png",
        selectedIconPath: "../../../images/31.png",
        text: "购物车",
        path: "/pages/mall/component/cart/cart" // 购物车页面的路径
      },
      {
        iconPath: "../../../images/42.png",
        selectedIconPath: "../../../images/41.png",
        text: "我的",
        path: "/pages/mall/component/user/user" // 我的页面的路径
      }
    ]
  },
  // 处理底部导航栏切换事件
  onTabChange: function (e) {
    const { index, pagePath } = e.detail;
    this.setData({ selectedIndex: index });
    // TODO: 实现页面跳转逻辑
    wx.navigateTo({
      url: pagePath
    });
  }
})
