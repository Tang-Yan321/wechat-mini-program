Page({
  redirectToMain: function () {
    wx.navigateTo({
      url: '/pages/main/main',
    });
  },
  redirectToNature: function () {
    wx.navigateTo({
      url: '/pages/activity/activity',
    });
  },

  redirectTomall: function () {
    wx.navigateTo({
      url: '/pages/mall/component/index',
    });
  },
  redirectTodetail: function () {
    wx.navigateTo({
      url: '/pages/pyq/circle/index',
    });
  },
  redirectTotest: function () {
    wx.navigateTo({
      url: '/pages/personal/personal',
    });
  },
  
});
