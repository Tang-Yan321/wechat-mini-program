// custom-tab-bar.js

Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  /**
   * 组件的属性列表
   */
  properties: {
    // 当前选中的页面索引
    selectedIndex: {
      type: Number,
      value: 0
    },
    // 页面列表
    pages: {
      type: Array,
      value: []
    }
  },

  /**
   * 组件的初始数据
   */
  data: {},

  /**
   * 组件的方法列表
   */
  methods: {
    // 点击底部导航栏按钮
    onTapTab: function (e) {
      const { index } = e.currentTarget.dataset;
      if (index !== this.data.selectedIndex) {
        // 获取点击的页面路径
        const pagePath = this.data.pages[index].path;
        // 触发自定义事件，通知父组件切换页面
        this.triggerEvent('change', { index, pagePath });
      }
    }
  }
})
