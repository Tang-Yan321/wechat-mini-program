Page({
  data: {
    imageUrls: [
      'https://bbs.qn.img-space.com/202404/1/2d7ec3111a2d6facb254b8cc6ba986da.jpg?imageView2/2/w/1024/q/100/ignore-error/1/&auth_key=1712053021-4902-0-e9316625d19b22d9cad242b9455a638b',
      'https://bbs.qn.img-space.com/202403/1/fda6eee4a5767ccb372a5224bf8e2afa.jpg?imageView2/2/w/1800/q/100/ignore-error/1/&auth_key=1712054190-5404-0-a6864930e965c3cfe930484194b7bc70',
      'https://www.xujc.com/uploads/img/2023/04/08/20230408201700.jpg',
      'https://bbs.qn.img-space.com/202311/30/965ff1308c1d907d53e2a2d228a89368.jpg?imageView2/2/w/1800/q/100/ignore-error/1/&auth_key=1712054251-7886-0-40b33f40a815d98b84113561f68a9533'
    ],
    current: 0,
    searchKeyword: '',
    searchResult: [],
    showResult: false
  },

  // 上一张轮播图
  prevSlide: function () {
    const current = this.data.current > 0 ? this.data.current - 1 : this.data.imageUrls.length - 1;
    this.setData({
      current: current
    });
  },

  // 下一张轮播图
  nextSlide: function () {
    const current = this.data.current < this.data.imageUrls.length - 1 ? this.data.current + 1 : 0;
    this.setData({
      current: current
    });
  },

  // 点击轮播图跳转到详情页
  goToDetail: function (event) {
    wx.navigateTo({
      url: '/pages/detail/detail'
    })
  },
// 单项活动详情页面的跳转
// 待处理！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！

  // 切换轮播图
  swiperChange: function (event) {
    const direction = event.currentTarget.dataset.direction; // 获取点击的方向
    if (direction === 'prev') {
      this.prevSlide(); // 调用上一张轮播图函数
    } else if (direction === 'next') {
      this.nextSlide(); // 调用下一张轮播图函数
    }
  },

// 活动分类跳转
// 全部活动
navigateToActivity_all: function() {
  wx.navigateTo({
    url: '/pages/activity/activity'
  })
},
// 正在进行的活动
navigateToActivity_runing: function() {
  wx.navigateTo({
    url: '/pages/activity_runing/activity_runing'
  })
},
// 活动申请跳转
navigateToActivity_application: function() {
    wx.navigateTo({
      url: '/pages/activity_application/activity_application'
    })
  },




});
