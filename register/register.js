Page({
  data: {
    username: "",
    password: "",
    confirmPassword: "",
    phone: "",
    email: ""
  },

  inputUsername(e) {
    this.setData({ username: e.detail.value });
  },

  inputPassword(e) {
    this.setData({ password: e.detail.value });
  },

  inputConfirmPassword(e) {
    this.setData({ confirmPassword: e.detail.value });
  },

  inputPhone(e) {
    this.setData({ phone: e.detail.value });
  },

  inputEmail(e) {
    this.setData({ email: e.detail.value });
  },

  register() {
    if (this.data.username && this.data.password && this.data.confirmPassword && this.data.phone && this.data.email) {
      if (this.data.password === this.data.confirmPassword) {
        // 处理注册逻辑，例如发送注册请求等
        console.log("用户名:", this.data.username);
        console.log("密码:", this.data.password);
        console.log("电话号码:", this.data.phone);
        console.log("邮箱地址:", this.data.email);
        wx.showToast({
          title: '注册成功',
          icon: 'success'
        });

        // 注册成功后跳转到index页面
        wx.navigateTo({
          url: '/pages/index/index',
        });
      } else {
        wx.showToast({
          title: '两次密码输入不一致',
          icon: 'none'
        });
      }
    } else {
      wx.showToast({
        title: '请填写完整信息',
        icon: 'none'
      });
    }
  },
});
