let col1H = 0;
let col2H = 0;
// 这里colH,col2H代表左右两片图片高度
Page({

    data: {
        scrollH: 0,//设置scroll-view占领的高度(屏幕高度screenHeight-导航栏高度-tabBar高度)
        imgWidth: 0,//设置初始图片宽度
        loadingCount: 0,//加载中的图片数量
        images: [],//加载中的图片
        col1: [],//左侧加载完成的图片
        col2: []//右侧加载完成的图片
    },

    onLoad: function () {
        wx.getSystemInfo({
            success: (res) => {
              // 第一部获取窗口高度windowHeight作为scroll-view的高度和图片的宽度
                let ww = res.windowWidth;
                let wh = res.windowHeight;
                let imgWidth = ww * 0.48;
                let scrollH = wh;
                this.setData({
                    scrollH: scrollH,
                    imgWidth: imgWidth
                });
              // 第二步加载图片
                this.loadImages();
            }
        })
    },
// 嗯这个函数是图片加载完成后触发,你有多少张图片他就触发多少此
    onImageLoad: function (e) {
      // 第三步图片加载完成触发此函数
      console.log("scroll=view触底触发");
        let imageId = e.currentTarget.id;
        let oImgW = e.detail.width;         //图片原始宽度
        let oImgH = e.detail.height;        //图片原始高度
        let imgWidth = this.data.imgWidth;  //图片设置的宽度
        let scale = imgWidth / oImgW;        //比例计算
        let imgHeight = oImgH * scale;      //自适应高度

        let images = this.data.images;
        let imageObj = null;
        for (let i = 0; i < images.length; i++) {
            let img = images[i];
            if (img.id === imageId) {
              console.log(img.id,imageId);
                imageObj = img;
                break;
            }
        }

        imageObj.height = imgHeight;

        let loadingCount = this.data.loadingCount - 1;
        let col1 = this.data.col1;
        let col2 = this.data.col2;

        if (col1H <= col2H) {
            col1H += imgHeight;
            col1.push(imageObj);
        } else {
            col2H += imgHeight;
            col2.push(imageObj);
        }

        let data = {
            loadingCount: loadingCount,
            col1: col1,
            col2: col2
        };

        if (!loadingCount) {
            data.images = [];
        }

        this.setData(data);
    },
// 这个是触底触发--加载更多图片
    loadImages: function () {
        let images = [
            { pic: "cloud://cloud1-4gzo8jr80dfccbbd.636c-cloud1-4gzo8jr80dfccbbd-1324808576/images/风景1.jpg", height: 0 },
            { pic: "cloud://cloud1-4gzo8jr80dfccbbd.636c-cloud1-4gzo8jr80dfccbbd-1324808576/images/风光2.jpg", height: 0 },
            { pic: "cloud://cloud1-4gzo8jr80dfccbbd.636c-cloud1-4gzo8jr80dfccbbd-1324808576/images/摄影1.jpg", height: 0 },
            { pic: "cloud://cloud1-4gzo8jr80dfccbbd.636c-cloud1-4gzo8jr80dfccbbd-1324808576/images/trees-3822149_1280.jpg", height: 0 },

        ];

        let baseId = "img-" + (+new Date());
           // 给图片添加id
        for (let i = 0; i < images.length; i++) {
            images[i].id = baseId + "-" + i;
        }

        this.setData({
            loadingCount: images.length, //加载中的图片数量
            images
        });
        console.log(this.data.images,this.data.loadingCount);
    }

});
